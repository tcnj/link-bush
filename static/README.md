# link-bush-latest

This repository holds the latest version [Link Bush](https://gitlab.com/tcnj/link-bush), ready for you to fork into your own deployment.

Alternatively, you can [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/tcnj/link-bush-latest)
