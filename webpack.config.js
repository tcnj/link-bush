const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')

module.exports = []

let devtool = process.env.DIST ? false : 'source-map'

if (process.env.DEV_WATCH || process.env.DIST)
    module.exports.push(
        {
            name: 'main',
            entry: {
                'public/cms': './src/cms.js'
            },
            output: {
                filename: '[name].js',
                path: path.resolve(__dirname, 'dist'),
            },
            plugins: [
                new MiniCssExtractPlugin({
                    filename: 'public/index.css'
                }),
                new CopyPlugin({
                    patterns: [
                        'static/'
                    ],
                }),
            ],
            module: {
                rules: [
                    {
                        test: /\.m?js$/,
                        exclude: /(node_modules|bower_components)/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: ['@babel/preset-env', '@babel/preset-react']
                            }
                        }
                    },
                    {
                        test: /\.scss$/,
                        exclude: /(node_modules|bower_components)/,
                        use: [
                            MiniCssExtractPlugin.loader,
                            'css-loader',
                            'sass-loader'
                        ]
                    }
                ]
            },
            devtool: devtool
        },
        {
            name: 'builder',
            target: 'node',
            entry: {
                builder: './src/builder.js'
            },
            output: {
                filename: '[name].js',
                path: path.resolve(__dirname, 'dist'),
            },
            module: {
                rules: [
                    {
                        test: /\.m?js$/,
                        exclude: /(node_modules|bower_components)/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: [
                                    ['@babel/preset-env', {
                                        useBuiltIns: "entry",
                                        targets: { node: "current" }
                                    }],
                                    '@babel/preset-react'
                                ]
                            }
                        }
                    }
                ]
            },
            devtool: devtool
        }
    )
if(process.env.NETLIFY_CMS || process.env.DIST)
    module.exports.push({
        entry: './src/netlify-cms.js',
        output: {
            filename: 'public/netlify-cms.js',
            path: path.resolve(__dirname, 'dist'),
        },
        plugins: [],
        module: {
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react']
                        }
                    }
                }
            ]
        },
        devtool: false
    })