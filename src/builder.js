import fs from 'fs'
import path from 'path'

import React from 'react'
import ReactDOMServer from 'react-dom/server'
import {minify} from 'html-minifier-terser'

import Bush from './bush'

const fsPromises = fs.promises

async function loadTemplates() {
    const mainTemplate = await fsPromises.readFile(path.resolve('./template.html'), 'utf8')
    const indexTemplate = await fsPromises.readFile(path.resolve('./public/index.html'), 'utf8')
    return [mainTemplate, indexTemplate]
}

function identity(v) { return v }

let matchJson = /\.json$/ig
let matchAdmin = /^admin($|\/)/ig

loadTemplates()
    .then(async ([mainTemplate, indexTemplate]) => {
        const basePath = path.resolve('./bushes/')
        const publicDir = path.resolve('./public')
        const files = await fsPromises.readdir(basePath)
        for( const file of files ) {
            const fullPath = path.join(basePath, file)

            const stat = await fsPromises.stat(fullPath)

            if(!stat.isFile()) {
                console.warn('%s is not a file, skipping', file)
                continue
            }
            if(!matchJson.exec(file)) {
                console.warn('%s does not end with .json, skipping', file)
                continue
            }

            const outName = file.replace(matchJson, '')

            if(matchAdmin.exec(outName)) {
                console.warn('%s attempts to overwrite /admin, skipping', file)
                continue
            }

            const content = await fsPromises.readFile(fullPath, 'utf8')
            const data = JSON.parse(content)

            const rendered = ReactDOMServer.renderToString(<Bush getAsset={identity} data={data} />)

            const template = outName === 'index' ? indexTemplate : mainTemplate
            const unminified = template
                .replace('<!-- TITLE -->', data.page_title)
                .replace('<!-- BODY -->', rendered)
            
            const htmlContent = minify(unminified, {
                collapseBooleanAttributes: true,
                collapseInlineTagWhitespace: true,
                collapseWhitespace: true,
                // conservativeCollapse: true,
                decodeEntities: true,
                minifyCSS: true,
                removeAttributeQuotes: true,
                removeComments: true,
                removeEmptyAttributes: true,
                removeOptionalTags: true,
                removeRedundantAttributes: true,
                removeStyleLinkTypeAttributes: true
            })

            const outPath = outName === 'index' ?
                path.join(publicDir, 'index.html') :
                path.join(publicDir, outName, 'index.html')
            
            if(outName !== 'index')
                if(await (fsPromises.access(path.join(publicDir, outName)).then(() => false).catch(() => true)))
                    await fsPromises.mkdir(path.join(publicDir, outName))
            
            await fsPromises.writeFile(outPath, htmlContent)

            console.log('Written %s', outPath)
        }
    })
