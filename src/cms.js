import {CMS, React} from './window'
import Bush from './bush'
import './style.scss'

class ColorWidget extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            defaulted: false
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(e) {
        this.props.onChange(e.target.value)
    }
    render() {
        if(!this.props.value && !this.state.defaulted) {
            this.setState({defaulted: true})
            this.props.onChange(this.props.field.getIn(['default']))
        }else if(this.props.value && this.state.defaulted) {
            this.setState({defaulted: false})
        }
        console.log(this.props)
        return <input
            className={this.props.classNameWrapper}
            style={{height: "3.5rem"}}
            type="color"
            value={this.props.value || this.props.field.getIn(['default'])}
            onChange={this.handleChange} />
    }
}

CMS.registerWidget('color', ColorWidget)

CMS.registerPreviewStyle('/index.css')
CMS.registerPreviewTemplate('bushes', function(props) {
    return <Bush data={props.entry.getIn(['data']).toJS()} getAsset={props.getAsset} />
})
