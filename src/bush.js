import React from 'react'

function get_style(data) {
    if(typeof data !== 'object')
        return {}
    return {"--primary": data.primary, "--secondary": data.secondary}
}

function profile_image(data, getAsset) {
    if(!data.enable)
        return null
    
    if(data.circle) {
        return <div className="profile-image profile-image--circle center">
                <img src={getAsset(data.url)} alt="Profile picture" />
            </div>
    }else{
        return <div className="profile-image center">
                <img src={getAsset(data.url)} alt="Profile picture" />
            </div>
    }
}

function links(data) {
    return data.map(data =>
        <li><a href={data.url}>{data.label === "" ? "&nbsp;" : data.label}</a></li>
    )
}

function Brand(props) {
    let children = props.children ? props.children.filter(v => v!==null) : null
    if(children && children.length > 0) {
        return <div className="brand center">
                <div className="brand__inner">
                    {children}
                </div>
            </div>
    }else{
        return null
    }
}

export default function(props) {
    let data = props.data
    let getAsset = props.getAsset

    let title = data.title.enable ? <h3>{ data.title.value }</h3> : null
    
    return <div style={get_style(data.styling)}>
        <Brand>
            { profile_image(data.profile_image, props.getAsset) }
            { title }
        </Brand>
        <div className="links center">
            <ul className="links__inner">
                { links(data.links || []) }
            </ul>
        </div>
    </div>
}